import { LightningElement, api, track, wire } from "lwc";

export default class Search extends NavigationMixin(LightningElement) {

	@track currency = 5000;
    @track currencyVal;
    
    connectedCallback() {
        this.currencyVal = this.numberWithCommas(this.currency);
    }
	numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    
  }

}